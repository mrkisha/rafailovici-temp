<?php

namespace Milos\RafailoviciBundle\Helpers\Storage\SolrCores;

use Milos\RafailoviciBundle\Entity\RafailoviciEntityInterface;
use Milos\RafailoviciBundle\Helpers\Storage\StorageInterface;

class BookingCore extends CoreAbstract implements StorageInterface {

    public function add(RafailoviciEntityInterface $booking)
    {
        $update = $this->client->createUpdate();

        $document = $update->createDocument();

        // get attributes for FULL CALENDAR
        $attributes = $this->getEventAttributes();

        $document->id = $booking->getId();
        $document->room = $booking->getRooms()->getRoomId();
        $document->allDay = true;
        $document->title = $booking->getRooms()->getRoomId() . 'Rez. ID: '. $booking->getId();
        $document->start = $booking->getStartDate();
        $document->end = $booking->getEndDate();
        $document->color = $attributes[$booking->getRooms()->getRoomId()]['color'];
        $document->textColor = $attributes[$booking->getRooms()->getRoomId()]['textColor'];
        $document->priority = $attributes[$booking->getRooms()->getRoomId()]['priority'];

        $documents[] = $document;

        $update->addDocuments($documents);
        $update->addCommit();

        $result = $this->client->update($update);
    }

    public function edit(RafailoviciEntityInterface $entity)
    {
        // TODO: Implement edit() method.
    }

    public function delete(RafailoviciEntityInterface $entity)
    {
        // TODO: Implement delete() method.
    }

    /**
     * Event attributes such as color, priority, etc
     *
     * @return array
     */
    public function getEventAttributes()
    {
        $attributes = [
            'A1' => [
                'color' => '#f7d842',
                'textColor' => 'black',
                'priority' => 10,

            ],
            'A2' => [
                'color' => '#98cb4a',
                'textColor' => 'black',
                'priority' => 9,
            ],
            'C1' => [
                'color' => '#f76d3c',
                'textColor' => 'black',
                'priority' => 8,
            ],
            'C2' => [
                'color' => '#00498f',
                'textColor' => 'black',
                'priority' => 7,
            ],
            'C3' => [
                'color' => '#00d6b2',
                'textColor' => 'black',
                'priority' => 6,
            ],
            'C4' => [
                'color' => '#C6E746',
                'textColor' => 'black',
                'priority' => 5,
            ],
            'D1' => [
                'color' => '#DA6179',
                'textColor' => 'black',
                'priority' => 4,
            ],
            'D2' => [
                'color' => '#9055FF',
                'textColor' => 'black',
                'priority' => 3,
            ],
        ];

        return $attributes;

    }

}
