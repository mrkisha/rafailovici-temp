<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 13.11.14
 * Time: 13:38
 */

namespace Milos\RafailoviciBundle\Helpers\Storage\SolrCores;


class CoreAbstract {
    /** @var \Solarium\Client $client */
    protected $client;

    public function __construct(\Solarium\Client $client){
        $this->client = $client;
    }
} 
