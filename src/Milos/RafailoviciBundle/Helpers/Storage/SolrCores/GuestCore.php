<?php

namespace Milos\RafailoviciBundle\Helpers\Storage\SolrCores;

use Doctrine\Common\Util\Debug;
use Milos\RafailoviciBundle\Entity\RafailoviciEntityInterface;
use Milos\RafailoviciBundle\Helpers\Storage\StorageInterface;

class GuestCore extends CoreAbstract implements StorageInterface {

    public function add(RafailoviciEntityInterface $entity)
    {
        $update = $this->client->createUpdate();

        $document = $update->createDocument();

        $document->id               = $entity->getId();
        $document->guestFirstName   = $entity->getGuestFirstName();
        $document->guestLastName    = $entity->getGuestLastName();
        $document->guestAddress_s   = $entity->getGuestAddress();
        $document->guestCity_s      = $entity->getGuestCity();
        $document->guestCountry_s   = $entity->getGuestCountry();
        $document->guestPhone_s     = $entity->getGuestPhone();
        $document->guestEmail_s     = $entity->getGuestEmail();
        $document->guestDocumentType_s   = $entity->getGuestDocumentType();
        $document->guestDocumentNumber_s = $entity->getGuestDocumentNumber();
        $document->guestNotesPositive_s  = $entity->getGuestNotesPositive();
        $document->guestNotesNegative_s  = $entity->getGuestNotesNegative();
        $document->createdAt_dt          = $entity->getCreatedAt();

        $documents[] = $document;

        $update->addDocuments($documents);
        $update->addCommit();

        $result = $this->client->update($update);
    }

    /**
     * Its same as add because solr overwrites data with same id
     * This way we keep it more semantic
     *
     * @param RafailoviciEntityInterface $entity
     */
    public function edit(RafailoviciEntityInterface $entity)
    {
        $this->add($entity);
    }

    public function delete(RafailoviciEntityInterface $entity)
    {

        Debug::dump($entity);
        $update = $this->client->createUpdate();

        $id = $entity->getId();


        // add the delete query and a commit command to the update query
//        $update->addDeleteById($id);

        $update->addDeleteQuery('id:'.$id);
        $update->addCommit();

        $result = $this->client->update($update);
    }
}
