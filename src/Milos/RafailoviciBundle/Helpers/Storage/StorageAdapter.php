<?php

namespace Milos\RafailoviciBundle\Helpers\Storage;

use Milos\RafailoviciBundle\Entity\RafailoviciEntityInterface;

class StorageAdapter implements StorageInterface
{

    /** array of storage Classes and storage engines (em, solr)*/
    private $storages;


    /**
     * Dynamically initiates storage classes and their engines
     *
     * @param array $storageServices
     */
    public function __construct(array $storageServices){

        foreach($storageServices as $storageService) {

            $storageClass = __NAMESPACE__.'\\'.ucfirst($storageService['storage']).'Storage';

            $this->storages[] = new $storageClass($storageService['storageEngine']);
        }
    }

    public function add(RafailoviciEntityInterface $entity){
        foreach ($this->storages as $storage) {
            $storage->add($entity);
        }
    }

    public function edit(RafailoviciEntityInterface $entity)
    {
        rsort($this->storages);
        foreach ($this->storages as $storage) {
            $storage->edit($entity);
        }
    }

    public function delete(RafailoviciEntityInterface $entity)
    {
        // must reverse the order because doctrine on delete removes entity id
        // so other engines cant access it and remove their data
        rsort($this->storages);
        foreach ($this->storages as $storage) {
            $storage->delete($entity);
        }
    }
}
