<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 31.10.14
 * Time: 00:22
 */

namespace Milos\RafailoviciBundle\Helpers\Storage;


use Doctrine\ORM\EntityManager;
use Milos\RafailoviciBundle\Entity\Guest;
use Nelmio\SolariumBundle\NelmioSolariumBundle;
use Solarium;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GuestsQueryHelper {

    /** @var Doctrine\ORM\EntityManager $em */
    private $em;
    /** @var Solarium\Client $solr */
    private $solr;

    private $rowsAll = 2000;

    public function __construct(EntityManager $em, $solr, ContainerInterface $container)
    {
        $this->em = $em;
        $this->solr = $solr;
    }

    /**
     * @return array Solarium\QueryType\Select\Result\Document
     */
    public function getAllGuests()
    {
        $query = $this->solr->createSelect();
        $query->setQuery('*:*');
        $query->setRows($this->rowsAll);

        $guests = $this->solr->select($query);

        return $guests->getDocuments();
    }

    public function deleteGuest($id)
    {
        // TODO: doctrine and solr
    }

    /**
     * @param int $id
     * @return Milos\RafaiiloviciBundle\Entity\Guest
     */
    public function getGuestById($id){
        $guest = $this->em->getRepository('MilosRafailoviciBundle:Guest')->find($id);

        return $guest;
    }

    public function submitGuest(Guest $guest){
        // orm
        $this->em->persist($guest);
        $this->em->flush();

        //solr
        $this->solrUpdateGuest($guest);
    }

    public function solrUpdateGuest(Guest $guest){
        $update = $this->solr->createUpdate();

        $document = $update->createDocument();
        
        $document->id             = $guest->getId();
        $document->guestFirstName = $guest->getGuestFirstName();
        $document->guestLastName  = $guest->getGuestLastName();
        $document->guestAddress_s   = $guest->getGuestAddress();
        $document->guestCity_s      = $guest->getGuestCity();
        $document->guestCountry_s   = $guest->getGuestCountry();
        $document->guestPhone_s     = $guest->getGuestPhone();
        $document->guestEmail_s     = $guest->getGuestEmail();
        $document->guestDocumentType_s   = $guest->getGuestDocumentType();
        $document->guestDocumentNumber_s = $guest->getGuestDocumentNumber();
        $document->guestNotesPositive_s  = $guest->getGuestNotesPositive();
        $document->guestNotesNegative_s  = $guest->getGuestNotesNegative();
        $document->createdAt_dt           = $guest->getCreatedAt();

        $documents[] = $document;

        $update->addDocuments($documents);
        $update->addCommit();

        $result = $this->solr->update($update);
    }

} 
