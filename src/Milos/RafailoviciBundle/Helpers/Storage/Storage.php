<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 10.11.14
 * Time: 23:51
 */

namespace Milos\RafailoviciBundle\Helpers\Storage;

use Doctrine\ORM\EntityManager;
use Milos\RafailoviciBundle\Entity\RafailoviciEntityInterface;
use Nelmio\SolariumBundle\ClientRegistry;

class Storage implements StorageInterface
{

    private $storage;

    /**
     * Solr by default shows 10 results,
     * need implicitly to declare any amount that is bigger than that.
     */
    private $rowsAll = 2000000;

    public function __construct(EntityManager $em, ClientRegistry $solrRegistry)
    {
        $this->storage = [
            [
                'storage' => 'doctrine',
                'storageEngine' => $em,
            ],
            [
                'storage' => 'solr',
                'storageEngine' => $solrRegistry,
            ],
        ];
    }

    /**
     * Add entity to storage
     *
     * @param RafailoviciEntityInterface $entity
     */
    public function add(RafailoviciEntityInterface $entity)
    {
        $storage = new StorageAdapter($this->storage);
        $storage->add($entity);
    }

    public function edit(RafailoviciEntityInterface $entity)
    {
        $storage = new StorageAdapter($this->storage);
        $storage->edit($entity);
    }


    public function delete(RafailoviciEntityInterface $entity)
    {
        $storage = new StorageAdapter($this->storage);
        $storage->delete($entity);
    }
}
