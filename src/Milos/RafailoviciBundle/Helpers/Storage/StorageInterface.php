<?php

namespace Milos\RafailoviciBundle\Helpers\Storage;

use Milos\RafailoviciBundle\Entity\RafailoviciEntityInterface;

interface StorageInterface {

    public function add(RafailoviciEntityInterface $object);

    public function edit(RafailoviciEntityInterface $object);

    public function delete(RafailoviciEntityInterface $object);

} 
