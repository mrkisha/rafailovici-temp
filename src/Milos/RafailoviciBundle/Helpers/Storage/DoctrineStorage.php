<?php

namespace Milos\RafailoviciBundle\Helpers\Storage;

use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManager;
use Milos\RafailoviciBundle\Entity\RafailoviciEntityInterface;

class DoctrineStorage implements StorageInterface {

    /** @var EntityManager $em */
    private $em;

    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    public function add(RafailoviciEntityInterface $entity)
    {
        $this->em->persist($entity);

        $this->flush();
    }

    /**
     * Its same as add because doctrine overwrites data if entity already exists
     * This way we keep it more semantic
     *
     * @param RafailoviciEntityInterface $entity
     */
    public function edit(RafailoviciEntityInterface $entity)
    {
        $entityName = strtolower(get_class($entity));
        $entityName = $this->extractEntityName($entityName);

        // create Solr Core Class name
        $doctrine = __NAMESPACE__.'\DoctrineStorage\\'.ucfirst($entityName).'Doctrine';

        // pass solr client to Solr Core Class
        $data = new $doctrine($this->em);

        // add Entity to Solr
        $data->edit($entity);
    }

    /**
     * Deletes Entity from DB
     *
     * @param RafailoviciEntityInterface $entity
     */
    public function delete(RafailoviciEntityInterface $entity)
    {
        $this->em->remove($entity);
        $this->flush();
    }

    /** flushes Entity */
    public function flush(){
        $this->em->flush();
    }

    private function extractEntityName($entityFullNameSpace){
        $entityFullNameArray = explode('\\', $entityFullNameSpace);

        return $entityFullNameArray[count($entityFullNameArray)-1];

    }



}
