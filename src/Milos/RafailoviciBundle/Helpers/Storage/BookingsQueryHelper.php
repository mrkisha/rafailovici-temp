<?php

namespace Milos\RafailoviciBundle\Helpers\Storage;

use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManager;
use Milos\RafailoviciBundle\Entity\Booking;
use Solarium\Client;

class BookingsQueryHelper {

    /** @var Doctrine\ORM\EntityManager $em */
    private $em;
    /** @var Solarium\Client $solr */
    private $solr;

    private $rowsAll = 2000;

    public function __construct(EntityManager $em, Client $solr)
    {
        $this->em = $em;
        $this->solr = $solr;
    }

    public function getAllBookings()
    {
        $query = $this->solr->createSelect();
        $query->setQuery('*:*');
        $query->setRows($this->rowsAll);

        $bookings = $this->solr->select($query);

        return $bookings->getDocuments();
    }

    public function submitBooking(Booking $booking)
    {

//        exit(Debug::dump($booking->getGuests()));
        // orm
        $this->em->persist($booking);
        $this->em->flush();

        //solr
        $this->solrUpdateBooking($booking);
    }

    private function solrUpdateBooking(Booking $booking){

        $update = $this->solr->createUpdate();

        $document = $update->createDocument();

        $attributes = $this->getEventAttributes();

        $document->id = $booking->getId();
        $document->room = $booking->getRooms()->getRoomId();
        $document->allDay = true;
        $document->title = $booking->getRooms()->getRoomId() . " Rez. ID: " . $booking->getId();
        $document->start = $booking->getStartDate();
        $document->end = $booking->getEndDate();
        $document->color = $attributes[$booking->getRooms()->getRoomId()]['color'];
        $document->textColor = $attributes[$booking->getRooms()->getRoomId()]['textColor'];
        $document->priority = $attributes[$booking->getRooms()->getRoomId()]['priority'];

        $documents[] = $document;

        $update->addDocuments($documents);
        $update->addCommit();

        $result = $this->solr->update($update);
    }

    public function getBookingById($id){
        return $this->em->getRepository('MilosRafailoviciBundle:Booking')->find($id);
    }


    /**
     * Event attributes such as color, priority, etc
     *
     * @return array
     */
    public function getEventAttributes()
    {
        $attributes = [
            'A1' => [
                'color' => '#f7d842',
                'textColor' => 'black',
                'priority' => 10,

            ],
            'A2' => [
                'color' => '#98cb4a',
                'textColor' => 'black',
                'priority' => 9,
            ],
            'C1' => [
                'color' => '#f76d3c',
                'textColor' => 'black',
                'priority' => 8,
            ],
            'C2' => [
                'color' => '#00498f',
                'textColor' => 'black',
                'priority' => 7,
            ],
            'C3' => [
                'color' => '#00d6b2',
                'textColor' => 'black',
                'priority' => 6,
            ],
            'C4' => [
                'color' => '#C6E746',
                'textColor' => 'black',
                'priority' => 5,
            ],
            'D1' => [
                'color' => '#DA6179',
                'textColor' => 'black',
                'priority' => 4,
            ],
            'D2' => [
                'color' => '#9055FF',
                'textColor' => 'black',
                'priority' => 3,
            ],
        ];

        return $attributes;

    }
} 
