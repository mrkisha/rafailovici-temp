<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 16.11.14
 * Time: 10:29
 */

namespace Milos\RafailoviciBundle\Helpers\Storage\DoctrineStorage;

use Milos\RafailoviciBundle\Entity\RafailoviciEntityInterface;
use Doctrine\Common\Util\Debug;

class BookingDoctrine extends DoctrineAbstract {

    /**
     * Its same as add because doctrine overwrites data if entity already exists
     * This way we keep it more semantic
     *
     * @param RafailoviciEntityInterface $entity
     */
    public function edit(RafailoviciEntityInterface $booking)
    {
        $ids = [];
        foreach($booking->getGuests() as $guest) {
            $ids[] = $guest->getId();
            $booking->getGuests()->removeElement($guest);
        }

        $this->flush();

        foreach($ids as $id) {
            $guest = $this->em->getRepository('MilosRafailoviciBundle:Guest')->find($id);
            $booking->addGuest($guest);
        }

        $this->flush();

    }

} 
