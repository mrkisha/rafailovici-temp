<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 16.11.14
 * Time: 10:29
 */

namespace Milos\RafailoviciBundle\Helpers\Storage\DoctrineStorage;

use Doctrine\ORM\EntityManager;
use Milos\RafailoviciBundle\Entity\RafailoviciEntityInterface;

class DoctrineAbstract {

    /** @var EntityManager $em */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function add(RafailoviciEntityInterface $entity)
    {
        $this->em->persist($entity);

        $this->flush();
    }

    /**
     * Its same as add because doctrine overwrites data if entity already exists
     * This way we keep it more semantic
     *
     * @param RafailoviciEntityInterface $entity
     */
    public function edit(RafailoviciEntityInterface $entity)
    {
        $this->add($entity);
    }

    /**
     * Deletes Entity from DB
     *
     * @param RafailoviciEntityInterface $entity
     */
    public function delete(RafailoviciEntityInterface $entity)
    {
        $this->em->remove($entity);
        $this->flush();
    }

    /** flushes Entity */
    public function flush(){
        $this->em->flush();
    }

} 
