<?php

namespace Milos\RafailoviciBundle\Helpers\Storage;

use Doctrine\Common\Util\Debug;
use Milos\RafailoviciBundle\Entity\RafailoviciEntityInterface;
use Nelmio\SolariumBundle\ClientRegistry;

class SolrStorage  implements StorageInterface{

    private $solrRegistry;
    private $entityName;

    public function __construct(ClientRegistry $solrRegistry)
    {
        $this->solr = $solrRegistry;
    }

    public function add(RafailoviciEntityInterface $entity)
    {
        $entityName = strtolower(get_class($entity));
        $entityName = $this->extractEntityName($entityName);

        // get solr core
        /** @var \Solarium\Client $client */
        $client = $this->solr->getClient($entityName);


        // create Solr Core Class name
        $solrCore = __NAMESPACE__.'\SolrCores\\'.ucfirst($entityName).'Core';

        // pass solr client to Solr Core Class
        $core = new $solrCore($client);

        // add Entity to Solr
        $core->add($entity);


    }

    public function edit(RafailoviciEntityInterface $entity)
    {
        $entityName = strtolower(get_class($entity));
        $entityName = $this->extractEntityName($entityName);

        // get solr core
        /** @var \Solarium\Client $client */
        $client = $this->solr->getClient($entityName);


        // create Solr Core Class name
        $solrCore = __NAMESPACE__.'\SolrCores\\'.ucfirst($entityName).'Core';

        // pass solr client to Solr Core Class
        $core = new $solrCore($client);

        // add Entity to Solr
        $core->edit($entity);
    }

    public function delete(RafailoviciEntityInterface $entity)
    {
        $entityName = strtolower(get_class($entity));
        $entityName = $this->extractEntityName($entityName);

        // get solr core
        /** @var \Solarium\Client $client */
        $client = $this->solr->getClient($entityName);

        // create Solr Core Class name
        $solrCore = __NAMESPACE__.'\SolrCores\\'.ucfirst($entityName).'Core';

        // pass solr client to Solr Core Class
        $core = new $solrCore($client);

        // add Entity to Solr
        $core->delete($entity);
    }

    private function extractEntityName($entityFullNameSpace){
        $entityFullNameArray = explode('\\', $entityFullNameSpace);

        return $entityFullNameArray[count($entityFullNameArray)-1];

    }


}
