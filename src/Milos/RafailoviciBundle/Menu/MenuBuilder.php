<?php

namespace Milos\RafailoviciBundle\Menu;

use Doctrine\Common\Util\Debug;
use Knp\Menu\FactoryInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;

class MenuBuilder extends ContainerAware {

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        $menu->addChild('Почетна', array('route' => 'milos_rafailovici_homepage'))
            ->setAttribute('icon', 'fa fa-home');

        $menu->addChild('Резервације')
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'fa fa-list-alt');
        $menu['Резервације']->addChild('Резервације', array('route' => 'milos_rafailovici_bookings'))
            ->setAttribute('icon', 'fa fa-file-text-o');
        $menu['Резервације']->addChild('Нова Резервација', array('route' => 'milos_rafailovici_booking_new'))
            ->setAttribute('icon', 'fa fa-file-text color-green');

        $menu->addChild('Гости')
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'fa fa-users');
        $menu['Гости']->addChild('Гости', array('route' => 'milos_rafailovici_guests'))
            ->setAttribute('icon', 'fa fa-users');
        $menu['Гости']->addChild('Нов Гост', array('route' => 'milos_rafailovici_guest_new'))
            ->setAttribute('icon', 'fa fa-file-o color-green');

        $this->setActiveParent($menu);

        return $menu;
    }

    public function userMenu(FactoryInterface $factory, array $options){
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

        $menu->addChild('Подешавања', array('label' => 'Подешавања'))
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'fa fa-cog');
        $menu['Подешавања']->addChild('Собе', array('route' => 'milos_rafailovici_homepage'))
            ->setAttribute('icon', 'fa fa-edit fa-fw');

        $menu->addChild('Корисник', array('label' => 'Корисник'))
            ->setAttribute('dropdown', true)
            ->setAttribute('icon', 'fa fa-user');

        $menu['Корисник']->addChild('Edit profile', array('route' => 'milos_rafailovici_homepage'))
            ->setAttribute('icon', 'fa fa-edit fa-fw');

        return $menu;
    }

    /**
     * Sets specified route as active in knpmenu
     * @See http://stackoverflow.com/questions/11407550/specify-current-route-or-template-as-active-link-knpmenu
     *
     * @param MenuItem $menu
     */
    private function setActiveParent(MenuItem $menu)
    {
        $requestRoute = $this->container->get('request')->getRequestUri();

        $guestEditRoute = $this->container->get('router')->generate('milos_rafailovici_guest_edit', [
            'id' => $this->container->get('request')->get('id') ? $this->container->get('request')->get('id') : '1'
        ]);

        $bookingEditRoute = $this->container->get('router')->generate('milos_rafailovici_booking_edit', [
            'id' => $this->container->get('request')->get('id') ? $this->container->get('request')->get('id') : '1'
        ]);

        switch ($requestRoute) {

            case $guestEditRoute:
                $menu->getChildren()['Гости']->setCurrent(true);
                break;

            case $bookingEditRoute:
                $menu->getChildren()['Резервације']->setCurrent(true);
                break;
        }

    }

} 
