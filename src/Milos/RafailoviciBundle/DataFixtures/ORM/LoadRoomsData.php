<?php

namespace Milos\RafailoviciBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Milos\RafailoviciBundle\Entity\Room;

class LoadRoomsData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $room1 = new Room();
        $room1->setRoomId('A1');
        $room1->setRoomDesc('Двокреветна соба');
        $room1->setMaxCapacity(2);
        $manager->persist($room1);

        $room2 = new Room();
        $room2->setRoomId('A2');
        $room2->setRoomDesc('Трокреветна соба');
        $room2->setMaxCapacity(3);
        $manager->persist($room2);

        $room3 = new Room();
        $room3->setRoomId('C1');
        $room3->setRoomDesc('Трокреветна соба');
        $room3->setMaxCapacity(3);
        $manager->persist($room3);

        $room4 = new Room();
        $room4->setRoomId('C2');
        $room4->setRoomDesc('Трокреветна соба');
        $room4->setMaxCapacity(3);
        $manager->persist($room4);

        $room5 = new Room();
        $room5->setRoomId('C3');
        $room5->setRoomDesc('Четворокреветна соба');
        $room5->setMaxCapacity(4);
        $manager->persist($room5);

        $room6 = new Room();
        $room6->setRoomId('C4');
        $room6->setRoomDesc('Четворокреветна соба');
        $room6->setMaxCapacity(4);
        $manager->persist($room6);

        $room7 = new Room();
        $room7->setRoomId('D1');
        $room7->setRoomDesc('Једнокреветна соба');
        $room7->setMaxCapacity(2);
        $manager->persist($room7);

        $room8 = new Room();
        $room8->setRoomId('D2');
        $room8->setRoomDesc('Петокреветни апартман');
        $room8->setMaxCapacity(5);
        $manager->persist($room8);

        $manager->flush();
    }
}
