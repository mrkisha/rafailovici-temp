<?php

namespace Milos\RafailoviciBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Milos\RafailoviciBundle\Entity\Guest;

class LoadGuestsData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $guest1 = new Guest();
        $guest1->setGuestFirstName('Jovan');
        $guest1->setGuestLastName('Jovanovic');
        $manager->persist($guest1);

        $guest2 = new Guest();
        $guest2->setGuestFirstName('Petar');
        $guest2->setGuestLastName('Petrovic');
        $manager->persist($guest2);

        $guest3 = new Guest();
        $guest3->setGuestFirstName('Milos');
        $guest3->setGuestLastName('Milosevic');
        $manager->persist($guest3);

        $guest4 = new Guest();
        $guest4->setGuestFirstName('Ivan');
        $guest4->setGuestLastName('Ivanovic');
        $manager->persist($guest4);

        $guest5 = new Guest();
        $guest5->setGuestFirstName('Atanas');
        $guest5->setGuestLastName('Atanasovic');
        $manager->persist($guest5);

        $guest6 = new Guest();
        $guest6->setGuestFirstName('Djuradj');
        $guest6->setGuestLastName('Djuradjovic');
        $manager->persist($guest6);

        $guest7 = new Guest();
        $guest7->setGuestFirstName('Viktor');
        $guest7->setGuestLastName('Viktorovic');
        $manager->persist($guest7);

        $guest8 = new Guest();
        $guest8->setGuestFirstName('Stefan');
        $guest8->setGuestLastName('Stefanovic');
        $manager->persist($guest8);

        $guest9 = new Guest();
        $guest9->setGuestFirstName('Nikola');
        $guest9->setGuestLastName('Nikolic');
        $manager->persist($guest9);

        $guest10 = new Guest();
        $guest10->setGuestFirstName('Nemanja');
        $guest10->setGuestLastName('Nemanjovic');
        $manager->persist($guest10);

        $manager->flush();
    }
}
