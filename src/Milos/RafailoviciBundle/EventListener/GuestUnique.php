<?php

namespace Milos\RafailoviciBundle\EventListener;

use Doctrine\Common\Util\Debug;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Milos\RafailoviciBundle\Entity\Booking;
use Milos\RafailoviciBundle\Entity\Guest;

class GuestUnique {

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if($entity instanceof Booking) {

            $entityManager = $args->getEntityManager();
            $guests = $entity->getGuests();

//            exit(Debug::dump($guests));
//            exit(111111111111111111111111111);

            foreach ($guests as $key => $guest) {
                $results = $entityManager->getRepository('MilosRafailoviciBundle:Guest')->findBy(array('id' => $guest->getId()));

                if(count($results) > 0) {
                    $guests[$key] = $results[0];
                }
            }
        }
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
//        $entity = $args->getEntity();
//
////        Debug::dump($entity->getGuests());
//
//        echo '==============================================';
//
//
//        if($entity instanceof Booking) {
////            echo '<pre>';
////            exit(Debug::dump($entity));
//
//            $entityManager = $args->getEntityManager();
//            $guests = $entity->getGuests();
//
////            Debug::dump($guests);
//
////            foreach ($guests as $guest) {
//////                Debug::dump($guest);
////                // check for existence of this guest
////                // find by params and sort by id to keep older guest first
////                $results = $entityManager->getRepository('MilosRafailoviciBundle:Guest')->findBy(array('id' => $guest->getId()));
////
////                // if guest exists at least 2 rows will be returned
////                // keep the first and discard the second
////                if (count($results) > 1) {
////
////                    $knownGuest = $results[0];
////                    $entity->addGuest($knownGuest);
////
////                    // remove duplicate guest
//////                    $duplicateGuest = $results[1];
//////                    $entityManager->remove($duplicateGuest);
////                } else {
////                    // guest does not exist yet, add relation
////                    $entity->addGuest($guest);
////                }
////            }
//
//            echo '=====================================';
//            Debug::dump($entity->getGuests());
//            exit();
//
//        }
//
//
    }

} 
