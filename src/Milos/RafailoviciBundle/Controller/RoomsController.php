<?php
namespace Milos\RafailoviciBundle\Controller;

use Milos\RafailoviciBundle\Entity\Rooms;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class RoomsController extends Controller
{

    public function showRoomAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('MilosRafailoviciBundle:Rooms');
        $room = $repository->findByRoomId($id);

        if (!$room) {
            throw $this->createNotFoundException('No room found with id '.$id);
        }

        return new Response(var_dump($room));
    }

    public function insertRoomsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $room = new Rooms();
        $room->setRoomId('C4');
        $room->setRoomDesc('Quadrupal bedroom');

        $em->persist($room);
        $em->flush();

        return new Response('Added new Room to db with Id ' . $room->getRoomId());
    //     $product = new Product();
    // $product->setName('A Foo Bar');
    // $product->setPrice('19.99');
    // $product->setDescription('Lorem ipsum dolor');

    // $em = $this->getDoctrine()->getManager();
    // $em->persist($product);
    // $em->flush();

    // return new Response('Created product id '.$product->getId());
    }
}
