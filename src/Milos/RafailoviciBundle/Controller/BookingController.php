<?php

namespace Milos\RafailoviciBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Debug;
use Doctrine\DBAL\DBALException;
use JMS\Serializer\SerializerBuilder;
use Milos\RafailoviciBundle\Entity\Booking;
use Milos\RafailoviciBundle\Entity\Guest;
use Milos\RafailoviciBundle\Form\Type\BookingEditType;
use Milos\RafailoviciBundle\Form\Type\BookingType;
use Milos\RafailoviciBundle\Messages\Alert;
use Milos\RafailoviciBundle\Messages\SuccessMessage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BookingController extends Controller
{

    public function newBookingAction(Request $request)
    {
        $booking = new Booking();

        $form = $this->createForm(new BookingType($this->getDoctrine()->getManager()), $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $helper = $this->get('storage.helper');
            $helper->add($booking);

            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add(
                'alert',
                Alert::renderAlert(new SuccessMessage(), [
                    'alertMessage' => 'Успешно',
                    'message' => sprintf(' сте додали нову резервацију!')
                ])
            );

            return $this->redirect($this->generateUrl('milos_rafailovici_bookings'), 301);
        }

        return $this->render('MilosRafailoviciBundle:Rafailovici/Booking:new.html.twig', [
            'formBooking' => $form->createView(),
            'guests' => $this->getGuestsAllAction(),
        ]);
    }

    public function bookingsAction()
    {
        // get all bookings
        $client = $this->get('query.helper.booking');
        $bookings = $client->getAllBookings();

        return $this->render('MilosRafailoviciBundle:Rafailovici/Booking:bookings.html.twig', [
            'bookings' => $bookings,
        ]);
    }

    public function editBookingAction(Request $request, Booking $booking){
        $helper = $this->get('storage.helper');



        $form = $this->createForm(new BookingEditType(), $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//
//            Debug::dump($booking);
//
//            $ids = [];
//            foreach($booking->getGuests() as $guest) {
//                $ids[] = $guest->getId();
//                $booking->getGuests()->removeElement($guest);
//            }
//
//            $em->flush();
//            Debug::dump($ids);
//
//            Debug::dump($booking);
//
//
//            foreach($ids as $id) {
//                $guest = $this->getDoctrine()->getManager()->getRepository('MilosRafailoviciBundle:Guest')->find($id);
//                $booking->addGuest($guest);
//            }
//
//            Debug::dump($booking);
//
//            $em->flush();
//
//            exit();

            $helper->edit($booking);

            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add(
                'alert',
                Alert::renderAlert(new SuccessMessage(), [
                    'alertMessage' => 'Успешно',
                    'message' => sprintf(' сте додали нову резервацију!')
                ])
            );

            return $this->redirect($this->generateUrl('milos_rafailovici_bookings'), 301);
        }

        return $this->render('MilosRafailoviciBundle:Rafailovici/Booking:new.html.twig', [
            'formBooking' => $form->createView(),
            'guests' => $this->getGuestsAllAction(),
        ]);
    }


    public function indexAction(Request $request)
    {
        $booking = new Booking();

        $form = $this->createForm(new BookingType(), $booking);

        // assign guest entities to booking if not POST
        if($request->getMethod() == 'POST') {

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $helper = $this->get('storage.helper');
                $helper->add($booking);

                $session = $this->getRequest()->getSession();
                $session->getFlashBag()->add(
                    'alert',
                    Alert::renderAlert(new SuccessMessage(), [
                        'alertMessage' => 'Успешно',
                        'message' => sprintf(' сте додали нову резервацију!')
                    ])
                );
                return $this->redirect($this->generateUrl('milos_rafailovici_homepage'), 301);
            }

        }
        return $this->render('MilosRafailoviciBundle:Rafailovici/Booking:index.html.twig',[
            'formBooking' => $form->createView(),
            'guests' => $this->getGuestsAllAction(),
        ]);

    }

    public function addGuestAction()
    {
        $em = $this->getDoctrine()->getManager();
        $guest = new Guest();
        // save guest
        $guest->setGuestFirstName('Љубодраг');
        $guest->setGuestLastName('Љубодраговић');
        $em->persist($guest);

        $em->flush();

        return new Response('Added guest');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function bookingAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $booking = new Booking();

        if ($request->getMethod() == 'POST') {
            $data = $request->request->all();
            $roomId = $data['roomId'];
            $from = $data['from'];
            $to = $data['to'];

            $guestFirstName = $data['guestFirstName'];
            $guestLastName = $data['guestLastName'];

            $results = $this->checkAvailability($roomId, $from, $to);

            if (count($results) == 1) {
                // save booking
                $booking->setRoomId($roomId);
                $booking->setStartDate($from);
                $booking->setEndDate($to);

                $response = new Response(json_encode($results));
                $response->headers->set('Content-Type', 'application/json');

                for ($i = 0; $i < count($guestFirstName); $i++) {
                    $guest = new Guest();
                    // save guest
                    $guest->setGuestFirstName($guestFirstName[$i]);
                    $guest->setGuestLastName($guestLastName[$i]);
                    $em->persist($guest);

                    // guest to booking
                    $booking->addGuest($guest);
                }

                $em->persist($booking);
                $em->flush();

                return $response;
            } else {
                $response = new Response(json_encode($results));
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }

        }
    }

    public function allBookingsAction(Request $request)
    {
        $get = $request->query->all();
        $start = $get['start'].'T00:00:00Z';
        $end = $get['end'].'T00:00:00Z';

//        echo '<pre>';
//        exit(var_dump($get));
        $serializer = SerializerBuilder::create()->build();

        $client = $this->get('solarium.client.booking');

        // get an update query instance
        $select = $client->createSelect();

        $select->setStart(0)->setRows(1000);
        $select->setQuery(
            'start:[* TO '.$end.']
            AND
            end: ['.$start.' TO *]'
        );

        $resultSet = $client->select($select);

        $results = [];
        foreach ($resultSet->getDocuments() as $doc) {
            $results[] = $doc->getFields();
        }


        $resultObjects = json_decode($serializer->serialize($results, 'json'));

        $response = new Response(json_encode($resultObjects));
        $response->headers->set('Content-Type', 'application/json');

        return $response;


    }

    /**
     * Event attributes such as color, priority, etc
     *
     * @return array
     */
    public function getEventAttributes()
    {
        $attributes = [
            'A1' => [
                'color' => '#f7d842',
                'textColor' => 'black',
                'priority' => 10,

            ],
            'A2' => [
                'color' => '#98cb4a',
                'textColor' => 'black',
                'priority' => 9,
            ],
            'C1' => [
                'color' => '#f76d3c',
                'textColor' => 'black',
                'priority' => 8,
            ],
            'C2' => [
                'color' => '#00498f',
                'textColor' => 'black',
                'priority' => 7,
            ],
            'C3' => [
                'color' => '#00d6b2',
                'textColor' => 'black',
                'priority' => 6,
            ],
            'C4' => [
                'color' => '#C6E746',
                'textColor' => 'black',
                'priority' => 5,
            ],
            'D1' => [
                'color' => '#DA6179',
                'textColor' => 'black',
                'priority' => 4,
            ],
            'D2' => [
                'color' => '#9055FF',
                'textColor' => 'black',
                'priority' => 3,
            ],
        ];

        return $attributes;

    }


    public function updateBookingAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            try {
                // update mysql
                $em = $this->getDoctrine()->getManager();

                $data = $request->request->all();
                foreach ($data as $key => $value) {
                    $$key = $value;
                }

                $booking = $em->getRepository('MilosRafailoviciBundle:Booking')->find($id);

                if (!$booking) {
                    throw $this->createNotFoundException('Није пронађена тражена резервација');
                }

                if (isset($startDate)) {
                    $booking->setStartDate($startDate);
                }
                if (isset($endDate)) {
                    $booking->setEndDate($endDate);
                }

                $em->flush();

                // update solr
                $client = $this->get('solarium.client.booking');
                $update = $client->createUpdate();

                $document = $update->createDocument();

                $document->id = $id;
                $document->room = $room;
                $document->allDay = $allDay;
                $document->title = $title;
                $document->start = new \DateTime($startDate);
                $document->end = new \DateTime($endDate);
                $document->color = $color;
                $document->textColor = $textColor;
                $document->priority = $priority;

                $documents[] = $document;

                $update->addDocuments($documents);
                $update->addCommit();

                $result = $client->update($update);

            } catch (\Doctrine\ORM\ORMException $e) {
                // TODO: should save it to log file
            } catch (Solarium\Exception $e) {
                // TODO: should save it to log file
            }

        } else {
            throw new \RuntimeException();
        }

        return new Response (json_encode($result));
    }

    public function checkAvailability($roomId, $from, $to)
    {

        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('MilosRafailoviciBundle:Booking')
            ->getAvailableRooms($roomId, $from, $to);

        return $data;
    }

    public function editBooking($id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $booking = $em->getRepository('MilosRafailoviciBundle:Booking')->find($id);

        if (!$booking) {
            throw $this->createNotFoundException('No booking found with booking id: ' . $id);
        }

        $originalGuests = new ArrayCollection();

        foreach ($booking->getGuest() as $guest) {
            $originalGuests->add($guest);
        }

        $editForm = $this->createForm(new BookingType(), $booking);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            foreach ($originalGuests as $guest) {
                if (false === $booking->getGuest()->contains($guest)) {
                    $guest->getBooking()->removeElement($guest);

                    $em->persist($guest);
                }
            }

            $em->persist($booking);
            $em->flush();

            return $this->redirect($this->generateUrl('milos_rafailovici_homepage', array('id' => $id)));
        }

        return $this->render('MilosRafailoviciBundle:Rafailovici:index.html.twig');
    }

    public function getGuestsAllAction()
    {
        $serializer = $this->get('serializer');

        $client = $this->get('solarium.client.guest');
        $query = $client->createSelect();

        $fields = [
            "id",
            "guestFirstName",
            "guestLastName",
            "fullText",
        ];

        $query->setFields($fields);
        $query->setQuery('*:*');

        $guests = $client->select($query);

        // set json headers
        $response = new Response($this->solrFieldsToArray($guests->getDocuments(), 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    public function solrFieldsToArray(array $docs, $serialize = null)
    {
        $data = [];
        foreach($docs as $doc) {
            $data[] = $doc->getFields();
        }

        $serializer = $this->get('serializer');

        if($serialize != null) {
            $data = $serializer->serialize($data, $serialize);
        }

        return $data;
    }

}
