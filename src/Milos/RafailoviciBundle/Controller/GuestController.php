<?php

namespace Milos\RafailoviciBundle\Controller;

use Doctrine\Common\Util\Debug;
use Milos\RafailoviciBundle\Entity\Guest;
use Milos\RafailoviciBundle\Form\Type\GuestType;
use Milos\RafailoviciBundle\Messages\Alert;
use Milos\RafailoviciBundle\Messages\DangerMessages;
use Milos\RafailoviciBundle\Messages\Info;
use Milos\RafailoviciBundle\Messages\InfoMessages;
use Milos\RafailoviciBundle\Messages\SuccessMessage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class GuestController extends Controller
{
    public function guestsAction()
    {
        // get guests
        $client = $this->get('query.helper');
        $guests = $client->getAllGuests();

        return $this->render('MilosRafailoviciBundle:Rafailovici/Guest:guests.html.twig', [
            'guests' => $guests,
        ]);
    }

    public function editGuestAction(Request $request, Guest $guest)
    {
        $helper = $this->get('storage.helper');

        $form = $this->createForm(new GuestType(), $guest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $helper->edit($guest);

//            exit(Debug::dump($guest));
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add(
                'alert',
                Alert::renderAlert(new SuccessMessage(), [
                    'alertMessage' => 'Успешно',
                    'message' => sprintf(' сте изменили податке за %s %s.', $guest->getGuestFirstName(), $guest->getGuestLastName())
                ])
            );

            return $this->redirect($this->generateUrl('milos_rafailovici_guests'), 301);
        }

        return $this->render('MilosRafailoviciBundle:Rafailovici/Guest:edit.html.twig', [
            'guest' => $form->createView(),
        ]);
    }

    public function newGuestAction(Request $request){
        $helper = $this->get('storage.helper');

        $guest = new Guest();

        $form = $this->createForm(new GuestType(), $guest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $helper->add($guest);

            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add(
                'alert',
                Alert::renderAlert(new SuccessMessage(), [
                    'alertMessage' => 'Успешно',
                    'message' => sprintf(' сте додали новог гостa!')
                ])
            );

            return $this->redirect($this->generateUrl('milos_rafailovici_guests'), 301);
        }

        $response = $this->render('MilosRafailoviciBundle:Rafailovici/Guest:new.html.twig', [
            'guest' => $form->createView(),
        ]);

        $date = new \DateTime();
        $date->modify('+600 seconds');

        $response->setExpires($date);

        return $response;
    }

    public function deleteGuestAction($id)
    {
        /** @var Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $guest = $em->getRepository('MilosRafailoviciBundle:Guest')->find($id);

        if($guest) {
            $helper = $this->get('storage.helper');
            $helper->delete($guest);

            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add(
                'alert',
                Alert::renderAlert(new SuccessMessage(), [
                    'alertMessage' => 'Успешно',
                    'message' => sprintf(' сте избрисали гостa (ид: <b>'.$id.'</b>)!')
                ])
            );

            return $this->redirect($this->generateUrl('milos_rafailovici_guests'), 301);
        } else {
            $session = $this->getRequest()->getSession();
            $session->getFlashBag()->add(
                'alert',
                Alert::renderAlert(new DangerMessages(), [
                    'alertMessage' => 'Грешка',
                    'message' => sprintf(', гост (ид: <b>'.$id.'</b>) који сте хтели да обришете не постоји!')
                ])
            );

            return $this->redirect($this->generateUrl('milos_rafailovici_guests'), 301);
        }
    }

    /**
     * @param $guest Guest form
     * @return Response
     */
    public function renderFormAction($guest)
    {
        $response = $this->render('MilosRafailoviciBundle:Rafailovici/Guest:formGuest.html.twig', [
            'guest' => $guest
        ]);

        $response->setSharedMaxAge(600);

        return $response;
    }

}
