<?php

namespace Milos\RafailoviciBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Milos\RafailoviciBundle\Form\Type\GuestType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookingType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST')
            ->add('startDate', 'text', array(
                'required' => false
            ))
            ->add('endDate', 'text', array(
                    'required' => false
                ))
            ->add('rooms', 'entity', array(
                'class' => 'MilosRafailoviciBundle:Room',
                'empty_value' => '',
                'required' => true,
                'error_bubbling' => false
            ))
            ->add('guests', 'collection', array(
                'type'      => new GuestInBookingType(),
                'required' => true,
                'allow_add' => true,

            ))
            ->add('bookingSubmit', 'submit')
        ;
    }

    public function setDefaultOption(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Milos\RafailoviciBundle\Entity\Booking'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'booking';
    }
}
