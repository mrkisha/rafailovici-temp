<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 07.11.14
 * Time: 16:06
 */

namespace Milos\RafailoviciBundle\Form\Type;

use Milos\RafailoviciBundle\Form\Type\GuestType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookingEditType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST')
            ->add('startDate', 'date', array(
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'y-MM-dd',
                'required' => false
            ))
            ->add('endDate', 'date', array(
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'y-MM-dd',
                'required' => false
            ))
            ->add('rooms', 'entity', array(
                'class' => 'MilosRafailoviciBundle:Room',
                'empty_value' => '',
                'required' => true,
                'error_bubbling' => false
            ))
            ->add('guests', 'collection', array(
                'type'      => new GuestInBookingType(),
                'required' => true,
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
            ))
            ->add('bookingSubmit', 'submit')
        ;
    }

    public function setDefaultOption(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Milos\RafailoviciBundle\Entity\Booking',
            'cascade_validation' => true,
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'booking';
    }
}
