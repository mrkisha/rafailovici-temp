<?php

namespace Milos\RafailoviciBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GuestType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options){

        // convert \DateTime object to string for rendering in form as text
        $transformer = new DateTimeToStringTransformer();

        $builder->setMethod('POST')
            ->add('id', 'hidden')
            ->add('guestFirstName')
            ->add('guestLastName')
            ->add('guestAddress')
            ->add('guestCity')
            ->add('guestCountry')
            ->add('guestPhone')
            ->add('guestEmail')
            ->add('guestDocumentType')
            ->add('guestDocumentNumber')
            ->add('guestNotesPositive', 'textarea')
            ->add('guestNotesNegative', 'textarea')
            ->add($builder->create('createdAt', 'text')
                ->addModelTransformer($transformer)
            )
            ->add('guestSubmit', 'submit')
        ;
    }

    public function setDefaultOption(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Milos\RafailoviciBundle\Entity\Guest',
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'guest';
    }
}
