$(document).ready(function() {
    if(typeof initialEventsURL == 'undefined') { initialEventsURL = ''; }
    if(typeof removeGuestEmbedTableRow == 'undefined') { removeGuestEmbedTableRow = false; }
    if(typeof removeGuestEmbedTableRow == 'undefined') { removeGuestEmbedTableRow = false; }

    // FullCalendar
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month'
        },
        weekNumbers: true,
        editable: true,
        fixedWeekCount: false,
        eventResize: function( event, delta, revertFunc, jsEvent, ui, view ) {
            var pulledEvents = $('#calendar').fullCalendar( 'clientEvents');

            // get event that has same room as the resized event
            var eventObjects = jQuery.grep(pulledEvents, function( object ) {
                return object.room == event.room;
            });

            for (i = 0 ;  i < eventObjects.length; i++) {
                // if resized room number exists in the memory
                if(eventObjects[i].id != event.id || eventObjects.length == 1) {
                    // check if dates overlap
                    var range = moment().range(new Date(eventObjects[i].start), new Date(eventObjects[i].end));
                    var rangeOrig = moment().range(new Date(event.start), new Date(event.end));

                    if (rangeOrig.overlaps(range) && eventObjects.length > 1) {
                        revertFunc();
                    } else {
                        // submit change
                        var updateData = {
                            id: event.id,
                            room: event.room,
                            allDay: event.allDay,
                            title: event.title,
                            startDate: event.start.format("YYYY-MM-DD"),
                            endDate: event.end.format("YYYY-MM-DD"),
                            color: event.color,
                            textColor: event.textColor,
                            priority: event.priority
                        };

                        startLoading();
                        $.ajax({
                            type: "POST",
                            url: eventUpdateURL, // comes from newBooking.html.twig file
                            data: $.param(updateData),// serializes the parameters being passed to class method into GET string.
                            success: function(data) {
                                stopLoading();
                            }
                        });
                    }

                }

            }

        },
        eventDrop: function( event, delta, revertFunc, jsEvent, ui, view ) {
            var pulledEvents = $('#calendar').fullCalendar( 'clientEvents');

            // get event that has same room as the resized event
            var eventObjects = jQuery.grep(pulledEvents, function( object ) {
                return object.room == event.room;
            });

            for (i = 0 ;  i < eventObjects.length; i++) {
                // if dragged room number exists in the memory
                if(eventObjects[i].id != event.id || eventObjects.length == 1) {
                    // check if dates overlap
                    var range = moment().range(new Date(eventObjects[i].start), new Date(eventObjects[i].end));
                    var rangeOrig = moment().range(new Date(event.start), new Date(event.end));

                    if (rangeOrig.overlaps(range) && eventObjects.length > 1) {
                        revertFunc();
                    } else {
                        // submit change
                        var updateData = {
                            id: event.id,
                            room: event.room,
                            allDay: event.allDay,
                            title: event.title,
                            startDate: event.start.format("YYYY-MM-DD"),
                            endDate: event.end.format("YYYY-MM-DD"),
                            color: event.color,
                            textColor: event.textColor,
                            priority: event.priority
                        };

                        startLoading();
                        $.ajax({
                            type: "POST",
                            url: eventUpdateURL, // comes from newBooking.html.twig file
                            data: $.param(updateData),// serializes the parameters being passed to class method into GET string.
                            success: function(data) {
                                stopLoading();
                            }
                        });

                    }

                }

            }

        },
        selectable: function( start, end, jsEvent, view ){
            // keep this one in order for calendar to be selectable
        },
        select: function( start, end, jsEvent, view ) {
            $('#booking_startDate').val(moment(start).format('YYYY-MM-DD'));
            $('#booking_endDate').val(moment(end).format('YYYY-MM-DD'));
        },
        events: {
            url: initialEventsURL,
            cache: false
        }

    });
    // End FullCalendar

    // close modal on escape
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            $('#myModal').modal('hide');
        }
    };

    // datepicker for input fields
    $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });

    //console.log(allGuests);
    var guests;

    $(document).on('getAllGuests', function (){
        // get guests from json
        $.getJSON('http://localhost.dev/rafailovici/web/app_dev.php/json/getGuestsAll', function(data){
            guests = data;
            $(document).trigger('guests-selectize');
        });
    });

    if(typeof bookingFormGuests != 'undefined' ) {
        $(document).trigger('getAllGuests');
    }

    // guest collection holder
    var $collectionHolder = $('table.embedded-booking-guests-table');
    // get the number of guests total to the creating booking
    $collectionHolder.data('index', $collectionHolder.find('.embedded-booking-guest').length);
    // where to append new guest form
    var $appendTo = $('.embedded-booking-guests-table  tbody');


    $(document).on('guests-selectize', function() {


        var $select = $('.search-existing-guest').selectize({
            persist: false,
            maxItems: 6,
            valueField: 'id',
            labelField: 'guestLastName',
            searchField: ['fullText'],
            options: guests,
            render: {
                option: function(item, escape) {
                    var label = item.guestFirstName + ' ' + item.guestLastName;
                    var caption = item.fullText;

                    return '<div>'+
                            '<span class="label">' + escape(label) + '</span>'+
                            '<span class="caption">' + escape(caption) + '</span>' +
                        '</div>';
                }
            },
            onItemAdd: function(item, arg) { // on item added
                console.log(item);
                var $guest = $select[0].selectize.options[item];
                addGuestTableRow($collectionHolder, $appendTo, $guest);
            },
            onItemRemove: function(item, arg) { // on item removed
                $('input[value="'+$select[0].selectize.options[item].id+'"]').parent().remove();
            }
        });



        if(removeGuestEmbedTableRow == true && (typeof someVar == 'undefined')) {
            console.log(2);
            addGuestsToSearchExistingGuest($select);
        }

        if(someVar == true) {
            $( ".embedded-booking-guest input:hidden" ).each(function( index ) {
                var id = $(this).val();
                var element = $(this);

                console.log(id);

                // remove tabele row so it wont be added twice
                // becase of onItemAdd event
                element.parent().remove();

                // add item to selectize input field
                $select[0].selectize.addItem(id);
            });
        }

    });
    // observers =======================================================================================================

    // end of observers ================================================================================================
    //Showing 1 to 10 of 10 entries
    var serbianTranslation = {
        search: "Тражи: ",
        lengthMenu:   "Прикажи по _MENU_ редова",
        info:         "Приказано _START_ - _END_ од укупно _TOTAL_ редова",
        zeroRecords:  "Није нађен ни један тражени податак",
        infoEmpty:    "Приказано 0 - 0 од 0 редова",
        infoFiltered: "(филтрирано од укупно _MAX_ редова)",
        infoPostFix:  "",
        pageLength:   25,
        paginate: {
            first:    "Почетна",
            previous: "Претходна",
            next:     "Следећа",
            last:     "Последња"
        }
    };

    $('table.guests-table').DataTable({
        paging: true,
        autoWidth: true,
        language: serbianTranslation
    });


});

function addGuestsToSearchNoRemoveTable($select) {
    var id = [];
    var removeRow = [];
    $( ".embedded-booking-guest input:hidden" ).each(function( index ) {
        id.push($(this).val());

        removeRow.push($(this));

        // remove tabele row so it wont be added twice
        // becase of onItemAdd event
        //element.parent().remove();

        // add item to selectize input field
        //$select[0].selectize.addItem(id);
    });

    for(i=0; i<id.length; i++) {
        $select[0].selectize.addItem(id[i]);

    }

}

/**
 * Add guest to input if form validation returns errors and guests were selected
 *
 * @param $select Object selectize object
 * @param $controllerVar boolean weather table row should (true) or should not (false) be added
 */
function addGuestsToSearchExistingGuest($select) {
    $( ".embedded-booking-guest input:hidden" ).each(function( index ) {
        var id = $(this).val();
        var element = $(this);

        // remove tabele row so it wont be added twice
        // becase of onItemAdd event
        element.parent().remove();

        // add item to selectize input field
        $select[0].selectize.addItem(id);
    });
}

/**
 *
 * @param $collectionHolder table that holds guest collection input fields
 * @param $appendTo element to append table tbody row
 * @param $guest guest object
 */
function addGuestTableRow($collectionHolder, $appendTo, $guest) {
    // get prototype HTML
    var $prototype = $('.embedded-booking-guests-table').data('prototype');
    // get the new index
    var $index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var $newGuestForm = $prototype.replace(/__name__/g, $index);

    // fill data into prototype

    var temp = $('<span/>');
    $(temp).html($newGuestForm);

    var inputs = $(temp).find("input");

    // place all ids in array
    var ids = [];
    inputs.each(function(index) {
        ids.push($(this).attr('id'));
    });

    // increase the index with one for the next item
    $collectionHolder.data('index', $index + 1);

    $appendTo.append($newGuestForm);

    var loopIndex = 0;
    $.each($guest, function(index, entry) {
        if (loopIndex < ids.length) {
            //console.log(index);
            //console.log(ids[loopIndex]);

            $('#'+ids[loopIndex]).val(entry);
        }
        loopIndex++;
    });

}

function startLoading() {
    $('html').prepend('<div class="ajaxLoad"><i class="fa fa-refresh fa-spin"></i></div>');
    $('body').css('overflow', 'hidden');
}

function stopLoading() {
    $('body').css('overflow', 'auto');
    $('.ajaxLoad').remove();
}

