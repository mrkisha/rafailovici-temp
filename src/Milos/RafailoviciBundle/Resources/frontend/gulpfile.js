
/**
 * Make a symbolic link of the bower_components
 * ex. fontawesome with paht fontawesome/scss in scss folder where main scss file will be
 * (../bower_components/fontawesome/scss)
 *
 * Then you can @import components in you scss file
 *
 * INSTALL packages
 * npm install glob gulp-ruby-sass gulp-minify-css gulp-jshint gulp-concat gulp-uglifyjs gulp-css-rebase-urls gulp-rename gulp-sass gulp-clean gulp-plumber --save-dev
 */

// Include gulp
var gulp = require('gulp');

// Include Our Plugins
// var livereload = require('gulp-livereload');
var concat = require('gulp-concat');
var glob = require('glob');
var jshint = require('gulp-jshint');
var minifyCSS = require('gulp-minify-css');
var rebaseUrls = require('gulp-css-rebase-urls');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var uglify = require('gulp-uglifyjs');
var clean = require('gulp-clean');
var plumber = require('gulp-plumber');


var jsFolders = [
    'bower_components/jquery/jquery.js',
    'bower_components/jquery-ui/jquery-ui.js',
    'bower_components/foundation/js/foundation.js',
    'bower_components/moment/min/moment.min.js',
    'bower_components/moment-range/lib/moment-range.bare.js',
    'bower_components/fullcalendar/dist/fullcalendar.js',
    'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js',
    'bower_components/selectize/dist/js/standalone/selectize.js',
    'bower_components/DataTables/media/js/jquery.dataTables.js',
    'js_include/datatable-bootstrap-3.js',
    'js/main.js'
];

var sass_include_paths = [
    'scss/main.scss'
];

var sass_watch = ['scss/**/*.scss', 'scss/*.scss'];

// Lint Task
gulp.task('lint', function() {
    return gulp.src(jsFolders)
        .pipe(plumber())
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

var replace = require('gulp-replace');

// Compile Sass
gulp.task('sass'/*, ['clean']*/, function() {
    return gulp.src(sass_include_paths)
        .pipe(plumber())
        .pipe(sass())
        .pipe( concat('main.css') )
        .pipe(gulp.dest('css'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(minifyCSS())
        .pipe(gulp.dest('../public/css'));
});

// Concatenate & Minify JS
gulp.task('scripts'/*, ['clean']*/, function() {
    return gulp.src(jsFolders)
        .pipe(plumber())
        .pipe(concat('all.js'))
        .pipe(gulp.dest('js'))
        .pipe(rename('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../public/js'));
});

// Watch Files For Changes
gulp.task('watch', function() {

    gulp.watch('js/main.js', [/*'lint',*/ 'scripts']);

    gulp.watch(sass_watch, ['sass']);

});

// Default Task
gulp.task('default', [/*'lint',*/ 'sass', 'scripts', 'watch']);

