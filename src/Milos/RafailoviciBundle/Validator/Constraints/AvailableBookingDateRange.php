<?php

namespace Milos\RafailoviciBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class AvailableBookingDateRange extends Constraint
{
    public $message = 'Соба %string% је заузета за период од Ж до Ж!';

    public function validatedBy()
    {
        return 'available_booking';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
} 
