<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 06.11.14
 * Time: 10:34
 */

namespace Milos\RafailoviciBundle\Validator\Constraints;


use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ValidatorException;

class AvailableBookingDateRangeValidator extends ConstraintValidator {

    /** @var Doctrine\ORM\EntityManager $em */
    private $em;

    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param object $object              The value that should be validated
     * @param Constraint $constraint    The constraint for the validation
     *
     * @api
     */
    public function validate($object, Constraint $constraint)
    {
        $roomId = $object->getRooms();

//        exit(Debug::dump($object));

        // Validate room
        if($roomId == null) {
            $this->context->addViolationAt('rooms', 'Must choose room!');
        }

        //Validate guests
        if(count($object->getGuests()) == 0) {
            $this->context->addViolationAt('guests', 'Please select guests!');
        }

//        if($object->getStartDate() instanceof \DateTime
//            && $object->getEndDate() instanceof \DateTime
//            && $object->getId() == null) {
//
//            $startDate = $object->getStartDate()->format('Y-m-d');
//            $endDate = $object->getEndDate()->format('Y-m-d');
//
//            $query = $this->em->getRepository('MilosRafailoviciBundle:Booking')->roomAvailability($roomId, $startDate, $endDate);
//
//            // if returns results that means that asked room is not awailable for that period
//            if(count($query)) {
//                $this->context->addViolationAt('startDate', 'Соба '.getRooms.' је заузета за тражени период!');
//                $this->context->addViolationAt('endDate', 'Соба '.getRooms.' је заузета за тражени период!');
//            }
//        }

    }

}
