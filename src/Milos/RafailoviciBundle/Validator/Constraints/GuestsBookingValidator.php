<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 06.11.14
 * Time: 23:31
 */

namespace Milos\RafailoviciBundle\Validator\Constraints;

use Doctrine\Common\Util\Debug;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class GuestsBookingValidator extends ConstraintValidator {

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     *
     * @api
     */
    public function validate($value, Constraint $constraint)
    {
        if(count($value) == 0) {
            $this->context->addViolationAt('guests', $constraint->message);
        }
    }
}
