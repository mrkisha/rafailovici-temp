<?php

namespace Milos\RafailoviciBundle\Command;

use Doctrine\Common\Util\Debug;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SolrIndexCommand extends ContainerAwareCommand 
{
    protected function configure()
    {
        $this
            ->setName('milos:solr:index')
            ->setDescription('Index data to Solr')
            ->addArgument('entity', InputArgument::REQUIRED, 'Which entity do you want to index into Solr?')
            ->addOption('clear', 'c',  InputOption::VALUE_NONE, 'Clear index before updating')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doCleanup = false;
        $doOptimize = false;

        $entity = $input->getArgument('entity');

        //get options from commandline
        $action = $input->getOption('clear');

        if ($input->getOption('clear')) $doCleanup = true;
//        if ($input->getOption('optimize')) {
//            $doOptimize = true;
//        }

        // get solr service guest core
        $core = 'solarium.client.'.$entity;
        $client = $this->getContainer()->get($core);

        // clear index
        if($doCleanup) $output->writeln($this->clearIndex($client));

        if($this->indexEntity($client, $entity)) {
            $output->writeln("");
            $output->writeln("<fg=white;bg=cyan>\n     ". ucfirst($entity) ." indexed!\n</fg=white;bg=cyan>");
        }

    }

    private function indexEntity($client, $entity)
    {
        $method = "index".ucfirst($entity);
        $this->$method($client);

        return true;
    }


    private function indexGuest($client)
    {
        // get doctrine and fetch guests
        $em = $this->getContainer()->get('doctrine')->getManager();
        $guests = $em->getRepository('MilosRafailoviciBundle:Guest');
        $items = $guests->getAllGuests();

        // get an update query instance
        $update = $client->createUpdate();

        $documents = [];

        foreach ($items as $item) {
            foreach ($item as $key => $value) {
                $document = $update->createDocument();

                $document->id             = $item['id'];
                $document->guestFirstName = $item['guestFirstName'];
                $document->guestLastName  = $item['guestLastName'];
                $document->guestAddress_s   = $item['guestAddress'];
                $document->guestCity_s      = $item['guestCity'];
                $document->guestCountry_s   = $item['guestCountry'];
                $document->guestPhone_s     = $item['guestPhone'];
                $document->guestEmail_s     = $item['guestEmail'];
                $document->guestDocumentType_s   = $item['guestDocumentType'];
                $document->guestDocumentNumber_s = $item['guestDocumentNumber'];
                $document->guestNotesPositive_s  = $item['guestNotesPositive'];
                $document->guestNotesNegative_s  = $item['guestNotesNegative'];
                $document->createdAt_dt           = new \DateTime($item['createdAt']);
            }
            $documents[] = $document;
        }

        $update->addDocuments($documents);
        $update->addCommit();

        return $client->update($update);
    }

    private function indexBooking($client)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $data = $em->getRepository('MilosRafailoviciBundle:Booking')->findAll();
        $serializer = SerializerBuilder::create()->build();

        // get an update query instance
        $update = $client->createUpdate();

        $resultObjects = json_decode($serializer->serialize($data, 'json'));

//        exit(var_dump($resultObjects));

        $methods = [
            'A1' => [
                'color' => '#f7d842',
                'textColor' => 'black',
                'priority' => 10,
            ],
            'A2' => [
                'color' => '#98cb4a',
                'textColor' => 'black',
                'priority' => 9,
            ],
            'C1' => [
                'color' => '#f76d3c',
                'textColor' => 'black',
                'priority' => 8,
            ],
            'C2' => [
                'color' => '#00498f',
                'textColor' => 'black',
                'priority' => 7,
            ],
            'C3' => [
                'color' => '#00d6b2',
                'textColor' => 'black',
                'priority' => 6,
            ],
            'C4' => [
                'color' => '#C6E746',
                'textColor' => 'black',
                'priority' => 5,
            ],
            'D1' => [
                'color' => '#DA6179',
                'textColor' => 'black',
                'priority' => 4,
            ],
            'D2' => [
                'color' => '#9055FF',
                'textColor' => 'black',
                'priority' => 3,
            ],
        ];

        foreach ($resultObjects as $object) {
            $document = $update->createDocument();

            $doc = $update->createDocument();

            $document->id = $object->id;
            $document->room = $object->room->room_id;
            $document->allDay = true;
            $document->title = $object->room->room_id . " Rez. ID: " . $object->id;
            $document->start = new \DateTime($object->start_date);
            $document->end = new \DateTime($object->end_date);
            $document->color = $methods[$object->room->room_id]['color'];
            $document->textColor = $methods[$object->room->room_id]['textColor'];
            $document->priority = $methods[$object->room->room_id]['priority'];

            $documents[] = $document;
        }

        $update->addDocuments($documents);
        $update->addCommit();

        return $client->update($update);
    }

    public function clearIndex($client)
    {
        $update = $client->createUpdate();
        $update->addDeleteQuery('*:*');
        $update->addCommit();
        $client->update($update);

        return "\n   <info>  Index cleared.</info>";
    }

} 
