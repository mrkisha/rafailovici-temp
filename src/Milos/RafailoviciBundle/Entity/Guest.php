<?php

namespace Milos\RafailoviciBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Guests
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Milos\RafailoviciBundle\Entity\GuestRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Guest implements RafailoviciEntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection $bookings
     * @ORM\ManyToMany(targetEntity="Milos\RafailoviciBundle\Entity\Booking", mappedBy="guests")
     */
    protected $bookings;

    /**
     * @var string
     *
     * @ORM\Column(name="guestFirstName", type="string", length=50, nullable=true)
     *
     * @Assert\NotBlank
     */
    protected $guestFirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="guestLastName", type="string", length=50, nullable=true)
     *
     * @Assert\NotBlank
     */
    protected $guestLastName;

    /**
     * @var string
     *
     * @ORM\Column(name="guestAddress", type="string", length=255, nullable=true)
     */
    protected $guestAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="guestCity", type="string", length=50, nullable=true)
     */
    protected $guestCity;

    /**
     * @var string
     *
     * @ORM\Column(name="guestCountry", type="string", length=50, nullable=true)
     */
    protected $guestCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="guestPhone", type="string", length=20, nullable=true)
     */
    protected $guestPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="guestEmail", type="string", length=100, nullable=true)
     */
    protected $guestEmail;

    /**
     * @var integer
     *
     * @ORM\Column(name="guestDocumentType", type="integer", nullable=true)
     */
    protected $guestDocumentType;

    /**
     * @var string
     *
     * @ORM\Column(name="guestDocumentNumber", type="string", length=30, nullable=true)
     */
    protected $guestDocumentNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="guestNotesPositive", type="string", length=500, nullable=true)
     */
    protected $guestNotesPositive;

    /**
     * @var string
     *
     * @ORM\Column(name="guestNotesNegative", type="string", length=500, nullable=true)
     */
    protected $guestNotesNegative;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    protected $createdAt;

//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(name="modifiedAt", type="datetime")
//     */
//    protected $modifiedAt;
//
//    public function __construct() {
//        $this->bookings = new ArrayCollection();
//    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
//        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));

        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set guestFirstName
     *
     * @param string $guestFirstName
     * @return Guest
     */
    public function setGuestFirstName($guestFirstName)
    {
        $this->guestFirstName = $guestFirstName;

        return $this;
    }

    /**
     * Get guestFirstName
     *
     * @return string 
     */
    public function getGuestFirstName()
    {
        return $this->guestFirstName;
    }

    /**
     * Set guestLastName
     *
     * @param string $guestLastName
     * @return Guest
     */
    public function setGuestLastName($guestLastName)
    {
        $this->guestLastName = $guestLastName;

        return $this;
    }

    /**
     * Get guestLastName
     *
     * @return string 
     */
    public function getGuestLastName()
    {
        return $this->guestLastName;
    }

    /**
     * Set guestAddress
     *
     * @param string $guestAddress
     * @return Guest
     */
    public function setGuestAddress($guestAddress)
    {
        $this->guestAddress = $guestAddress;

        return $this;
    }

    /**
     * Get guestAddress
     *
     * @return string 
     */
    public function getGuestAddress()
    {
        return $this->guestAddress;
    }

    /**
     * Set guestCity
     *
     * @param string $guestCity
     * @return Guest
     */
    public function setGuestCity($guestCity)
    {
        $this->guestCity = $guestCity;

        return $this;
    }

    /**
     * Get guestCity
     *
     * @return string 
     */
    public function getGuestCity()
    {
        return $this->guestCity;
    }

    /**
     * Set guestCountry
     *
     * @param string $guestCountry
     * @return Guest
     */
    public function setGuestCountry($guestCountry)
    {
        $this->guestCountry = $guestCountry;

        return $this;
    }

    /**
     * Get guestCountry
     *
     * @return string 
     */
    public function getGuestCountry()
    {
        return $this->guestCountry;
    }

    /**
     * Set guestPhone
     *
     * @param string $guestPhone
     * @return Guest
     */
    public function setGuestPhone($guestPhone)
    {
        $this->guestPhone = $guestPhone;

        return $this;
    }

    /**
     * Get guestPhone
     *
     * @return string 
     */
    public function getGuestPhone()
    {
        return $this->guestPhone;
    }

    /**
     * Set guestEmail
     *
     * @param string $guestEmail
     * @return Guest
     */
    public function setGuestEmail($guestEmail)
    {
        $this->guestEmail = $guestEmail;

        return $this;
    }

    /**
     * Get guestEmail
     *
     * @return string 
     */
    public function getGuestEmail()
    {
        return $this->guestEmail;
    }

    /**
     * Set guestDocumentType
     *
     * @param boolean $guestDocumentType
     * @return Guest
     */
    public function setGuestDocumentType($guestDocumentType)
    {
        $this->guestDocumentType = $guestDocumentType;

        return $this;
    }

    /**
     * Get guestDocumentType
     *
     * @return boolean 
     */
    public function getGuestDocumentType()
    {
        return $this->guestDocumentType;
    }

    /**
     * Set guestDocumentNumber
     *
     * @param string $guestDocumentNumber
     * @return Guest
     */
    public function setGuestDocumentNumber($guestDocumentNumber)
    {
        $this->guestDocumentNumber = $guestDocumentNumber;

        return $this;
    }

    /**
     * Get guestDocumentNumber
     *
     * @return string 
     */
    public function getGuestDocumentNumber()
    {
        return $this->guestDocumentNumber;
    }

    /**
     * Set guestNotesPositive
     *
     * @param string $guestNotesPositive
     * @return Guest
     */
    public function setGuestNotesPositive($guestNotesPositive)
    {
        $this->guestNotesPositive = $guestNotesPositive;

        return $this;
    }

    /**
     * Get guestNotesPositive
     *
     * @return string 
     */
    public function getGuestNotesPositive()
    {
        return $this->guestNotesPositive;
    }

    /**
     * Set guestNotesNegative
     *
     * @param string $guestNotesNegative
     * @return Guest
     */
    public function setGuestNotesNegative($guestNotesNegative)
    {
        $this->guestNotesNegative = $guestNotesNegative;

        return $this;
    }

    /**
     * Get guestNotesNegative
     *
     * @return string 
     */
    public function getGuestNotesNegative()
    {
        return $this->guestNotesNegative;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Guest
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Guest
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Add bookings
     *
     * @param \Milos\RafailoviciBundle\Entity\Booking $bookings
     * @return Guest
     */
    public function addBooking(\Milos\RafailoviciBundle\Entity\Booking $bookings)
    {
        if(!$this->bookings->contains($bookings)) {
            $this->bookings->add($bookings);
            $bookings->addGuest($this);
        }

        return $this;
    }

    /**
     * Remove bookings
     *
     * @param \Milos\RafailoviciBundle\Entity\Booking $bookings
     */
    public function removeBooking(\Milos\RafailoviciBundle\Entity\Booking $bookings)
    {
        if($this->bookings->contains($bookings)) {
            $this->bookings->removeElement($bookings);
            $bookings->removeGuest($this);
        }


    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    public function toSolrDocument($doc)
    {
        $doc->id             = $this->getId();
        $doc->guestFirstName = $this->getGuestFirstName();
        $doc->guestLastName  = $this->getGuestLastName();

        return $doc;
    }
}
