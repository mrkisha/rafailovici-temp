<?php

namespace Milos\RafailoviciBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Bookings
 *
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Milos\RafailoviciBundle\Entity\BookingRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Bookings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Milos\RafailoviciBundle\Entity\BookingsGuests", mappedBy="booking")
     */
    protected $bookingsGuests;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="date")
     */
    protected $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="date")
     */
    protected $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="roomId", type="string", nullable=true)
     */
    protected $roomId;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    public function __construct() {
        $this->$bookingsGuests = new ArrayCollection();
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));

        }
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookingsGuests
     *
     * @param integer $bookingsGuests
     * @return Bookings
     */
    public function setBookingsGuests($bookingsGuests)
    {
        $this->bookingsGuests = $bookingsGuests;

        return $this;
    }

    /**
     * Get bookingsGuests
     *
     * @return integer 
     */
    public function getBookingsGuests()
    {
        return $this->bookingsGuests;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Bookings
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Bookings
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set roomId
     *
     * @param string $roomId
     * @return Bookings
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;

        return $this;
    }

    /**
     * Get roomId
     *
     * @return string 
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Bookings
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return Bookings
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }
}
