<?php

namespace Milos\RafailoviciBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Milos\RafailoviciBundle\Validator\Constraints as MilosAssert;

/**
 * Bookings
 *
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Milos\RafailoviciBundle\Entity\BookingRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @MilosAssert\AvailableBookingDateRange
 */
class Booking implements RafailoviciEntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection $guests
     *
     * @ORM\ManyToMany(targetEntity="Milos\RafailoviciBundle\Entity\Guest", inversedBy="bookings", cascade={"persist"})
     * @ORM\JoinTable(name="Bookings_Guests",
     *      joinColumns={@ORM\JoinColumn(name="booking_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="guest_id", referencedColumnName="id")}
     * )
     *
     */
    protected $guests;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="date")
     *
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    protected $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="date")
     *
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    protected $endDate;

    /**
     * @var ArrayCollection $room
     *
     * @ORM\ManyToOne(targetEntity="Milos\RafailoviciBundle\Entity\Room", inversedBy="bookings")
     * @ORM\JoinColumn(name="roomId", referencedColumnName="id")
     *
     */
    protected $room;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    public function __construct()
    {
        $this->guests = new ArrayCollection();
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));

        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Booking
     */
    public function setStartDate($startDate)
    {
        $this->startDate = new \DateTime($startDate);

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Booking
     */
    public function setEndDate($endDate)
    {
        $this->endDate = new \DateTime($endDate);

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

//    /**
//     * Set roomId
//     *
//     * @param string $roomId
//     * @return Booking
//     */
//    public function setRoomId($roomId)
//    {
//        $this->roomId = $roomId;
//
//        return $this;
//    }
//
//    /**
//     * Get roomId
//     *
//     * @return string
//     */
//    public function getRoomId()
//    {
//        return $this->roomId;
//    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Booking
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return Booking
     */
    public function setModifiedAt(\Datetime $modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Add guests
     *
     * @param \Milos\RafailoviciBundle\Entity\Guest $guests
     * @return Booking
     */
    public function addGuest(\Milos\RafailoviciBundle\Entity\Guest $guests)
    {

//        $guests->addBooking($this);
        $this->guests[] = $guests;

//        if (!$this->guests->contains($guests)) {
//            $this->guests[] = $guests;
//            $guests->addBooking($this);
//        }
        return $this;
    }

    /**
     * Remove guests
     *
     * @param \Milos\RafailoviciBundle\Entity\Guest $guests
     */
    public function removeGuest(\Milos\RafailoviciBundle\Entity\Guest $guests)
    {
        $this->guests->removeElement($guests);
    }

//    public function hasGuest(Guest $guest){
//        return $this->getGuests()->contains($guest);
//    }

    /**
     * Get guests
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGuests()
    {
        return $this->guests;
    }

    /**
     * Set room
     *
     * @param \Milos\RafailoviciBundle\Entity\Room $room
     * @return Booking
     */
    public function setRooms(\Milos\RafailoviciBundle\Entity\Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \Milos\RafailoviciBundle\Entity\Room 
     */
    public function getRooms()
    {
        return $this->room;
    }
}
