<?php

namespace Milos\RafailoviciBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rooms
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Room
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     *
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="roomId", type="string")
     *
     * @Assert\NotBlank
     */
    private $roomId;


    /**
     *
     * @ORM\OneToMany(targetEntity="Milos\RafailoviciBundle\Entity\Booking", mappedBy="room")
     *
     * @Assert\NotBlank
     *
     */
    private $bookings;

    /**
     * @var string
     *
     * @ORM\Column(name="roomDesc", type="string", length=255)
     */
    private $roomDesc;

    /**
     * @var integer
     *
     *@ORM\Column(name="maxCapacity", type="integer", length=1)
     */
    private $maxCapacity;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    public function __construct(){
        $this->bookings = new ArrayCollection();
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if($this->getCreatedAt() == null)
        {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roomId
     *
     * @param string $roomId
     * @return Rooms
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;

        return $this;
    }

    /**
     * Get roomId
     *
     * @return string
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Set roomDesc
     *
     * @param string $roomDesc
     * @return Rooms
     */
    public function setRoomDesc($roomDesc)
    {
        $this->roomDesc = $roomDesc;

        return $this;
    }

    /**
     * Get roomDesc
     *
     * @return string
     */
    public function getRoomDesc()
    {
        return $this->roomDesc;
    }

    /**
     * Sets the value of id.
     *
     * @param integer $id the id
     *
     * @return self
     */
    public function _setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Sets the value of roomId.
     *
     * @param string $roomId the room id
     *
     * @return self
     */
    public function _setRoomId($roomId)
    {
        $this->roomId = $roomId;

        return $this;
    }

    /**
     * Sets the value of roomDesc.
     *
     * @param string $roomDesc the room desc
     *
     * @return self
     */
    public function _setRoomDesc($roomDesc)
    {
        $this->roomDesc = $roomDesc;

        return $this;
    }

    /**
     * Gets the value of maxCapacity.
     *
     * @return integer
     */
    public function getMaxCapacity()
    {
        return $this->maxCapacity;
    }

    /**
     * Sets the value of maxCapacity.
     *
     * @param integer $maxCapacity the max capacity
     *
     * @return self
     */
    public function setMaxCapacity($maxCapacity)
    {
        $this->maxCapacity = $maxCapacity;

        return $this;
    }

    /**
     * Gets the value of created_at.
     *
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Sets the value of created_at.
     *
     * @param mixed $created_at the created  at
     *
     * @return self
     */
    protected function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Gets the value of modified_at.
     *
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Sets the value of modified_at.
     *
     * @param mixed $modified_at the modified  at
     *
     * @return self
     */
    protected function setModifiedAt($modified_at)
    {
        $this->modified_at = $modified_at;

        return $this;
    }

    /**
     * Add bookings
     *
     * @param \Milos\RafailoviciBundle\Entity\Booking $bookings
     * @return Room
     */
    public function addBooking(\Milos\RafailoviciBundle\Entity\Booking $bookings)
    {
        $this->bookings[] = $bookings;

        return $this;
    }

    /**
     * Remove bookings
     *
     * @param \Milos\RafailoviciBundle\Entity\Booking $bookings
     */
    public function removeBooking(\Milos\RafailoviciBundle\Entity\Booking $bookings)
    {
        $this->bookings->removeElement($bookings);
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBookings()
    {
        return $this->bookings;
    }


    /**
     * Override toString() method to return the name of the room
     * @return string roomId
     */
    public function __toString()
    {
        return $this->roomId;
    }
}
