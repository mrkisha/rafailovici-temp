<?php

namespace Milos\RafailoviciBundle\Entity;

use Doctrine\ORM\EntityRepository;

class BookingRepository extends EntityRepository
{

    public function getAvailableRooms($roomId, $from, $to)
    {

        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare("SELECT r.roomId, r.maxCapacity
                        FROM Room r
                        WHERE r.roomId
                            NOT IN (
                                SELECT b.roomId FROM Booking b
                                WHERE NOT (
                                    b.endDate < :from
                                    OR
                                    b.startDate > :to
                                )
                            )
                            AND r.roomId = :roomId
                        ORDER BY r.roomId");
        $stmt->bindValue(':from', $from);
        $stmt->bindValue(':to', $to);
        $stmt->bindValue(':roomId', $roomId);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;

    }

    /**
     * @param string $roomId       Room Id
     * @param string $startDate    Start date of the booking (Y-m-d)
     * @param string $endDate      End date of booking (Y-m-d)
     *
     * @return array
     */
    public function roomAvailability($roomId, $startDate, $endDate)
    {
        $query = $this->getEntityManager()
            ->createQuery("SELECT b FROM Milos\RafailoviciBundle\Entity\Booking b
                                WHERE b.roomId = :roomId
                                  AND :startDate < b.endDate
                                  AND :endDate > b.startDate ")
            ->setParameters(['roomId' => $roomId, 'startDate' => $startDate, 'endDate' => $endDate]);

        return $query->getResult();
    }

}
