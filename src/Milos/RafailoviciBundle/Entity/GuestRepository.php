<?php

namespace Milos\RafailoviciBundle\Entity;

use Doctrine\ORM\EntityRepository;

class GuestRepository extends EntityRepository {

    /**
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllGuests()
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare(
                "SELECT *
                    FROM Guest g
                    ORDER BY g.guestLastName, g.guestFirstName"
            );
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

}
