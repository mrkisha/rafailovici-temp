<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 03.11.14
 * Time: 22:06
 */

namespace Milos\RafailoviciBundle\Messages;


class InfoMessages extends MessagesAbstract {

    /**
     * @var string
     */
    public $alertType = 'alert alert-info';
} 
