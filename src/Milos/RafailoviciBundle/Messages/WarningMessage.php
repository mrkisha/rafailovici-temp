<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 03.11.14
 * Time: 20:22
 */

namespace Milos\RafailoviciBundle\Messages;


class WarningMessage extends MessagesAbstract {

    /**
     * @var string
     */
    public $alertType = 'alert alert-warning';

} 
