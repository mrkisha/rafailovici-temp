<?php

namespace Milos\RafailoviciBundle\Messages;

abstract class MessagesAbstract {

    /**
     * Returns Bootstrap3 alert component
     *
     * @param string $alertMessage
     * @param string $message
     *
     * @return string
     */
    public function renderMessage($alertMessage, $message){
        return sprintf('
            <div class="'.$this->alertType.' alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>%s</strong> %s
            </div>', $alertMessage, $message);
    }
} 
