<?php

namespace Milos\RafailoviciBundle\Messages;

class Alert {

    /**
     * @param MessagesAbstract $message
     * @param array $options
     *
     * @return string
     *
     * @throws \Exception
     */
    public static function renderAlert(MessagesAbstract $message, array $options)
    {
        if(!is_array($options) || count($options) == 0) {
            throw new \Exception(sprintf('Please provide options as an array for %s', __CLASS__));
        }

        return $message->renderMessage($options['alertMessage'], $options['message']);
    }

} 
