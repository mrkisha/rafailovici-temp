<?php

namespace Milos\RafailoviciBundle\Messages;


class SuccessMessage extends MessagesAbstract {

    /**
     * @var string
     */
    public $alertType = 'alert alert-success';

}
