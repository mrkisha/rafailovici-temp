<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 28.10.14
 * Time: 14:12
 */

namespace Milos\CliSolrBundle\Solr\FileText;


interface FileTextInterface {
    public function getText();
} 
