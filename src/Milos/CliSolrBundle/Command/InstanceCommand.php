<?php
namespace Milos\CliSolrBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\HttpFoundation\File\File;

class InstanceCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            ->setName('milos:solr:create-instance')
            ->setDescription('Crate Solr instance, for help add \'-h\'')
            /*->addArgument('entity', InputArgument::REQUIRED, 'Which entity do you want to index into Solr?')*/
            ->setHelp(<<<EOT
The <info>milos:solr:create-instance</info> command generates solr core.
EOT
)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // input solr root folder
        $solrRootDir = $this->checkSolrDir($input, $output);

        $output->writeln("
Solr root dir has been found in <info>{$solrRootDir}</info> directory.");

        $output->writeln('
Proceeding...');

        // create new solr instance (dir within the solr root dir - /opt/solr)
        $solrInstance = $this->checkSolrInstance($input, $output, $solrRootDir);

        $solrInstanceDir = $solrRootDir.'/'.$solrInstance;

        if($solrInstance !== true) {
            try {
                $this->createDir($solrInstanceDir);
            } catch (IOException $e) {
                $output->writeln("<error>
You do not have right permission to create folder in: {$solrRootDir}.

Please change directory permission and try again...
</error>
                ");
            }

        } else {
            throw new \Exception("Solr instance already exists! Please use another name...");
        }

        // create first level tree

        // README.txt
        $solrCoreReadme = $this->getText('milos_cli_solr.SolrInstanceReadme.readme');
        $this->createFile($solrInstanceDir, 'README.txt', $solrCoreReadme);

        // bin/
        $this->createDir($solrInstanceDir.'/bin');

        // solr.xml
        $solrXmlData = $this->getText('milos_cli_solr.core.xml');
        $this->createFile($solrInstanceDir, 'solr.xml', $solrXmlData);

        // zoo.cfg
        $zooCfgData = $this->getText('milos_cli_solr.zoo.cfg');
        $this->createFile($solrInstanceDir, 'zoo.cfg', $zooCfgData);

        // create core
        $this->createCore($input, $output, $solrInstanceDir);

        // TODO: at the end of the whole process ask for creating another core

        // TODO: wrapp everything in try and catch
        // methods should throw \Exception on errors
        // if error, delete whole solr folder


    }

    private function checkSolrDir($input, $output)
    {
        $helper = $this->getHelper('question');

        // solr start file
        $start = 'start.jar';

        $question = new Question('
<info>Type in the Solr root directory [<comment>/opt/solr</comment>]: </info>', "/opt/solr");

        $dir = rtrim($helper->ask($input, $output, $question), '/');

        if($startFile = new File($dir.'/'.$start)) {
            return $dir;
        } else {
            return false;
        }

    }

    private function checkSolrInstance($input, $output, $solrRootDir)
    {
        $filesystem   = $this->getContainer()->get('filesystem');

        $helper = $this->getHelper('question');

        $question = new Question('
<info>Type in the Solr instance directory: </info>');

        $dir = trim($helper->ask($input, $output, $question));

        if($filesystem->exists($solrRootDir.'/'.$dir)) {
            return true;
        } else {
            return $dir;
        }
    }

    private function createDir($dir)
    {
        $filesystem = $this->getContainer()->get('filesystem');
        $filesystem->mkdir($dir);
    }

    /**
     * @param $serviceName
     * @return mixed
     */
    private function getText($serviceName)
    {
        $readMe = $this->getContainer()->get($serviceName);

        return $readMe->getText();
    }

    /**
     * @param string $file Name of the file with extention
     * @param string $dir Full path dir structure where to place file
     * @param mixed $data Optional Place content in file
     */
    private function createFile($dir, $file, $data = false) {
        $filesystem = $this->getContainer()->get('filesystem');

        try {
            $filesystem->touch($dir.'/'.$file);
        } catch (IOException $e) {
            echo $e->getMessage();
        }

        if($data != false) file_put_contents($dir.'/'.$file, $data);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param string $solrInstanceDir
     */
    private function createCore(InputInterface $input, OutputInterface $output, $solrInstanceDir)
    {
        $helper = $this->getHelper('question');

        $question = new Question('
<info>Name of the core: </info>');

        $coreName = trim($helper->ask($input, $output, $question));

        $coreDir = $solrInstanceDir.'/'.$coreName;

        // create 1st level of structure
        $this->createCoreStructure($coreDir, $coreName);

        // fill /conf dir
        $this->confFiles($input, $output, $coreDir, $coreName);

        // fill /lang dir
        $this->langFiles($coreDir);

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return array
     */
    public function fieldQuestions(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        $anotherField = new Question("
<info>Do you want to create another field [<comment>y/n</comment>]: </info>");

        $answer = $helper->ask($input, $output, $anotherField);
        while($answer != 'y' && $answer != 'n') {
            $answer = $helper->ask($input, $output, $anotherField);
        }

        if($answer == 'y') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $coreDir
     * @param string $coreName
     */
    private function createCoreStructure($coreDir, $coreName)
    {
        $filesystem = $this->getContainer()->get('filesystem');

        // /conf
        $filesystem->mkdir($coreDir.'/conf');

        // README.txt
        $readmeData = $this->getText('milos_cli_solr.SolrCoreReademe.readme');
        $this->createFile($coreDir, 'README.txt', $readmeData);

        // core.properties
        $this->createFile($coreDir, 'core.properties', 'name='.$coreName);
    }

    /**
     * Creates all necessary files except schema.xml
     * that file is created separately
     *
     * @param string $coreDir Instance full path directory
     * @param string $coreName Core name
     */
    private function confFiles(InputInterface $input, OutputInterface $output, $coreDir, $coreName)
    {
        $filesystem = $this->getContainer()->get('filesystem');

        // solrconfig.xml
        $solrconfigXmlData = $this->getText('milos_cli_solr.SolrconfigXML');
        $this->createFile($coreDir.'/conf', 'solrconfig.xml', $solrconfigXmlData);

        // schema.xml
        $fields = [];

        $anotherField = true;

        while($anotherField === true) {
            $fields[] = $this->prepareFieldValues($input, $output);

            $anotherField = $this->fieldQuestions($input, $output);
        }

        $schemaXMLData = $this->schemaXMLFieldsData($fields);
        $this->createFile($coreDir.'/conf', 'schema.xml', $schemaXMLData);

        // stopwords.txt
        $stopwordsTXTData = $this->getText('milos_cli_solr.StopwordsTXT');
        $this->createFile($coreDir.'/conf', 'stopwords.txt', $stopwordsTXTData);

        // synonyms.txt
        $synonymsTXTData = $this->getText('milos_cli_solr.SynonymsTXT');
        $this->createFile($coreDir.'/conf', 'synonyms.txt', $synonymsTXTData);

        // protowords.txt
        $protowordsTXTData = $this->getText('milos_cli_solr.ProtwordsTXT');
        $this->createFile($coreDir.'/conf', 'protwords.txt', $protowordsTXTData);

    }

    public function langFiles($coreDir)
    {
        $filesystem = $this->getContainer()->get('filesystem');

        // create /lang folder
        $lang = 'lang';
        $filesystem->mkdir($coreDir.'/conf/'.$lang);

        // stopwords_en.txt
        $stopwordsEnTXTData = $this->getText('milos_cli_solr.StopwordsEnTXT');
        $this->createFile($coreDir.'/conf/'.$lang, 'stopwords_en.txt', $stopwordsEnTXTData);

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return array
     */
    private function prepareFieldValues(InputInterface $input, OutputInterface $output)
    {
        $attributeValues = [];

        $helper = $this->getHelper('question');

        $schemaHelper = $this->getContainer()->get('milos_cli_solr.SchemaXML');

        // get field attribute names
        $attributeNames = $schemaHelper->getAttrNames();

        foreach($attributeNames as $key => $value) {
            $question = new Question("<info>Please enter field {$key} [<comment>{$value}</comment>]: </info>");

            $data = $helper->ask($input, $output, $question);

            // TODO: add validation for input
            // add default options
            if($data == '') {
                $data = $value;
            }

            $attributeValues[$key] = $data;

        }

        return $attributeValues;
    }

    private function schemaXMLFieldsData(array $fields)
    {
        $schema = $this->getContainer()->get('milos_cli_solr.SchemaXML');

        $schema->setFields($fields);

        return $schema->getText();
    }
} 
