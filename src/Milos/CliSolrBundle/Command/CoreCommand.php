<?php
/**
 * Created by PhpStorm.
 * User: mrkisha
 * Date: 30.10.14
 * Time: 07:53
 */

namespace Milos\CliSolrBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class CoreCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            ->setName('milos:solr:create-core')
            ->setDescription('Crate Solr core to existing instance, for help add \'-h\'')
            ->setHelp(<<<EOT
The <info>milos:solr:create-core</info> command generates solr core to existing solr instance.
EOT
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $filesystem = $this->getContainer()->get('filesystem');

        $question = new Question('
<info>Type in the existing Solr instance root directory [<comment>/opt/solr/<core name></comment>]: </info>');

        $instanceDir = rtrim($helper->ask($input, $output, $question), '/');

        $output->writeln($instanceDir);

        if($filesystem->exists($instanceDir) == false) {
            throw new \Exception('Directory '.$instanceDir.' does not exist!');
        }

        // create core
        $this->createCore($input, $output, $instanceDir);

    }

    private function createDir($dir)
    {
        $filesystem = $this->getContainer()->get('filesystem');
        $filesystem->mkdir($dir);
    }

    /**
     * @param $serviceName
     * @return mixed
     */
    private function getText($serviceName)
    {
        $readMe = $this->getContainer()->get($serviceName);

        return $readMe->getText();
    }

    /**
     * @param string $file Name of the file with extention
     * @param string $dir Full path dir structure where to place file
     * @param mixed $data Optional Place content in file
     */
    private function createFile($dir, $file, $data = false) {
        $filesystem = $this->getContainer()->get('filesystem');

        try {
            $filesystem->touch($dir.'/'.$file);
        } catch (IOException $e) {
            echo $e->getMessage();
        }

        if($data != false) file_put_contents($dir.'/'.$file, $data);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param string $solrInstanceDir
     */
    private function createCore(InputInterface $input, OutputInterface $output, $solrInstanceDir)
    {
        $helper = $this->getHelper('question');
        $filesystem = $this->getContainer()->get('filesystem');

        $question = new Question('
<info>Name of the core: </info>');

        $coreName = trim($helper->ask($input, $output, $question));

        $coreDir = $solrInstanceDir.'/'.$coreName;

        $output->writeln($coreDir);

        while($filesystem->exists($coreDir) == true) {
            $output->writeln('<error>Core already exists, please choose another name for core.</error>');

            $coreName = trim($helper->ask($input, $output, $question));

            $coreDir = $solrInstanceDir.'/'.$coreName;
        }

        // create 1st level of structure
        $this->createCoreStructure($coreDir, $coreName);

        // fill /conf dir
        $this->confFiles($input, $output, $coreDir, $coreName);

        // fill /lang dir
        $this->langFiles($coreDir);

    }

    /**
     * @param string $coreDir
     * @param string $coreName
     */
    private function createCoreStructure($coreDir, $coreName)
    {
        $filesystem = $this->getContainer()->get('filesystem');

        // /conf
        $filesystem->mkdir($coreDir.'/conf');

        // README.txt
        $readmeData = $this->getText('milos_cli_solr.SolrCoreReademe.readme');
        $this->createFile($coreDir, 'README.txt', $readmeData);

        // core.properties
        $this->createFile($coreDir, 'core.properties', 'name='.$coreName);
    }

    /**
     * Creates all necessary files except schema.xml
     * that file is created separately
     *
     * @param string $coreDir Instance full path directory
     * @param string $coreName Core name
     */
    private function confFiles(InputInterface $input, OutputInterface $output, $coreDir, $coreName)
    {
        $filesystem = $this->getContainer()->get('filesystem');

        // solrconfig.xml
        $solrconfigXmlData = $this->getText('milos_cli_solr.SolrconfigXML');
        $this->createFile($coreDir.'/conf', 'solrconfig.xml', $solrconfigXmlData);

        // schema.xml
        $fields = [];

        $anotherField = true;

        while($anotherField === true) {
            $fields[] = $this->prepareFieldValues($input, $output);

            $anotherField = $this->fieldQuestions($input, $output);
        }

        $schemaXMLData = $this->schemaXMLFieldsData($fields);
        $this->createFile($coreDir.'/conf', 'schema.xml', $schemaXMLData);

        // stopwords.txt
        $stopwordsTXTData = $this->getText('milos_cli_solr.StopwordsTXT');
        $this->createFile($coreDir.'/conf', 'stopwords.txt', $stopwordsTXTData);

        // synonyms.txt
        $synonymsTXTData = $this->getText('milos_cli_solr.SynonymsTXT');
        $this->createFile($coreDir.'/conf', 'synonyms.txt', $synonymsTXTData);

        // protowords.txt
        $protowordsTXTData = $this->getText('milos_cli_solr.ProtwordsTXT');
        $this->createFile($coreDir.'/conf', 'protwords.txt', $protowordsTXTData);

    }

    public function langFiles($coreDir)
    {
        $filesystem = $this->getContainer()->get('filesystem');

        // create /lang folder
        $lang = 'lang';
        $filesystem->mkdir($coreDir.'/conf/'.$lang);

        // stopwords_en.txt
        $stopwordsEnTXTData = $this->getText('milos_cli_solr.StopwordsEnTXT');
        $this->createFile($coreDir.'/conf/'.$lang, 'stopwords_en.txt', $stopwordsEnTXTData);

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return array
     */
    private function prepareFieldValues(InputInterface $input, OutputInterface $output)
    {
        $attributeValues = [];

        $helper = $this->getHelper('question');

        $schemaHelper = $this->getContainer()->get('milos_cli_solr.SchemaXML');

        // get field attribute names
        $attributeNames = $schemaHelper->getAttrNames();

        foreach($attributeNames as $key => $value) {
            $question = new Question("<info>Please enter field {$key} [<comment>{$value}</comment>]: </info>");

            $data = $helper->ask($input, $output, $question);

            // TODO: add validation for input
            // add default options
            if($data == '') {
                $data = $value;
            }

            $attributeValues[$key] = $data;

        }

        return $attributeValues;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return array
     */
    public function fieldQuestions(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        $anotherField = new Question("
<info>Do you want to create another field [<comment>y/n</comment>]: </info>");

        $answer = $helper->ask($input, $output, $anotherField);
        while($answer != 'y' && $answer != 'n') {
            $answer = $helper->ask($input, $output, $anotherField);
        }

        if($answer == 'y') {
            return true;
        } else {
            return false;
        }
    }

    private function schemaXMLFieldsData(array $fields)
    {
        $schema = $this->getContainer()->get('milos_cli_solr.SchemaXML');

        $schema->setFields($fields);

        return $schema->getText();
    }

} 
